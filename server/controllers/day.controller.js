import DayModel from '../model/day.model.js';
import {decode} from "../utils/authHelpers.js";

const getAll = async (req, res) => {
    const {_id: userId} = decode(req.headers["authorization"])
    const allDays = await DayModel.find({owner: userId});

    if (allDays) {
        res.send(allDays);
    } else {
        res.status(404);
        res.send({message: 'smth went wrong'})
    }
}


const saveDay = async (req, res) => {
    const {_id: userId} = decode(req.headers["authorization"])
    const searchedDate = new Date(req.body.date);
    searchedDate.setHours(0, 0, 0, 0);

    const nextDay = new Date(req.body.date);
    nextDay.setDate(nextDay.getDate() + 1);
    nextDay.setHours(0, 0, 0, 0);

    const dayFromDB = await DayModel.findOne({
        date: req.body.date
    });

    if (!dayFromDB) {
        await DayModel.create({
            pleasures: [req.body.title],
            date: req.body.date,
            owner: userId,
        })
        res.send({status: true});
    } else {
        res.status(201);
        res.send({
            status: false,
            message: 'Day already exists'
        })
    }
};

const updateDay = async (req, res) => {
    const {_id: userId} = decode(req.headers["authorization"])
    const dayFromDB = await DayModel.findOne({date: req.body.date});

    if (dayFromDB && dayFromDB.owner.toString() === userId.toString()) {
        const {title: updatedTitle, description: updatedDescription} = req.body;

        if (dayFromDB.pleasures.includes(req.body.title)) {
            await DayModel.updateOne({...dayFromDB}, {
                pleasures: [...dayFromDB.pleasures.filter(item => item !== req.body.title)],
                title: updatedTitle,
                description: updatedDescription
            });
        } else {
            await DayModel.updateOne({...dayFromDB}, {
                pleasures: [...dayFromDB.pleasures, ...[req.body.title]],
                title: updatedTitle,
                description: updatedDescription
            });
        }

        res.send({status: true});
    } else {
        res.status(404);
        res.send({
            status: false,
            message: 'Day not found'
        })
    }
}

const deleteDay = async (req, res) => {
    const {_id: userId} = decode(req.headers["authorization"])
    const dayFromDB = await DayModel.findOne({_id: req.params.id});

    if (dayFromDB && dayFromDB.owner === userId) {
        const deleted = await DayModel.deleteOne({_id: dayFromDB['_id']});
        if (deleted.ok > 0) {
            res.send(dayFromDB);
        } else {
            res.status(204);
            res.send({message: 'something went wrong'});
        }
    } else {
        res.status(404);
        res.send({
            message: 'day not found'
        })
    }
}


const DayController = {
    getAll,
    save: saveDay,
    update: updateDay,
    delete: deleteDay
}
export default DayController;
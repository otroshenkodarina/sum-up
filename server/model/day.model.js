import mongoose from  'mongoose';
const Schema = mongoose.Schema

const DaySchema = new Schema({
  date: {
    type: Date,
    required: true
  },
  pleasures: [String],
  owner: {
    type: Schema.Types.ObjectId,
    link: 'User',
    required: true
  }
})

export default mongoose.model('DayV2', DaySchema);
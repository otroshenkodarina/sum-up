import {makeStyles} from "@material-ui/core";
import {getWeek} from '../../utils'

const useStyles = makeStyles(() => ({
    dates: {
        display: 'flex',
        gap: '50px',
        justifyContent: 'flex-end'
    }
}))

const Days = () => {
    const classes = useStyles();
    console.log('getWeek().date --> ',getWeek().date)
    return (
        <div className={classes.dates}>
            {getWeek().dayArr.map((item, key) => (
                    <div key={key} style={item === getWeek().date ? {color: 'red'} : {}}>
                    {item.toString() + getWeek().monthName}
                </div>

            ))}
        </div>
    );
}

export default Days;
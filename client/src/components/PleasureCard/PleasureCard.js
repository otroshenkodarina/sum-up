import React, {useEffect, useState} from 'react';
import Grid from "@material-ui/core/Grid";
import Card from "@material-ui/core/Card";
import Typography from "@material-ui/core/Typography";
import MyModal from "../Modal/MyModal";
import {useDispatch, useSelector} from "react-redux";
import {Checkbox, makeStyles} from "@material-ui/core";
import API from "../../utils/API";
import {pleasuresActions} from "../../redux/pleasures";
import {getWeek, getWeekNumber} from '../../utils';
import {AlarmOff} from "@material-ui/icons";
import {daysSelectors} from "../../redux/days";
import moment from 'moment';

const useStyles = makeStyles((theme) => ({
    cards: {
        display: 'flex',
        marginTop: '30px',
        gap: '18%',
        alignItems: 'center'
    },
    card: {
        width: '400px'
    },
    checkbox: {
        display: 'flex',
        gap: '50px'
    }
}))

const PleasureCard = ({card}) => {
    const dispatch = useDispatch();
    const [isOpened, setIsOpened] = useState(false);
    const [checkedDays, setCheckedDays] = useState([])
    const userState = useSelector(state => state.user);
    const daysState = useSelector(state => state.days);
    const {title, description} = card
    const classes = useStyles();

    const handleClose = () => setIsOpened(!isOpened)

    const handleEdit = async (form) => {
        await API.editPleasure(userState.token, form);
        handleClose();
        dispatch(pleasuresActions.getAll());
    }

    const handleChangeCheck = async (day) => {
        const date = new Date(`${new Date().getFullYear()}-${getWeek().monthName}-${getWeek().dayArr[day - 1]}`)

        const res = await API.saveDays(moment(date).format('YYYY-MM-DD'), card.title, userState.token);
        if (!res) await API.updateDays(moment(date).format('YYYY-MM-DD'), card.title, userState.token);

        if (checkedDays.includes(day)) setCheckedDays(checkedDays.filter(item => item !== day));
        else setCheckedDays([...checkedDays, ...[day]]);
    }

    useEffect(() => {
        const currentDay = daysState.filter(item => item.pleasures.includes(card.title));
        if (currentDay?.length) setCheckedDays(currentDay.map(item => new Date(item?.date).getDay()))
    }, [card, daysState]);

    return (
        <div>
            <div className={classes.cards}>
                <Card className={classes.card} variant="outlined">
                    <Typography
                        style={{cursor: 'pointer'}}
                        variant="h4"
                        color="textSecondary"
                        onClick={() => setIsOpened(!isOpened)}>
                        {title}
                    </Typography>
                    <Typography color="textSecondary">
                        {description}
                    </Typography>
                </Card>
                <div className={classes.checkbox}>
                    {
                        [1, 2, 3, 4, 5, 6, 7].map((item, key) => (
                            <Checkbox
                                key={key}
                                checked={!!checkedDays.includes(item)}
                                onChange={() => handleChangeCheck(item)}
                                inputProps={{'aria-label': 'Checkbox demo'}}
                            />
                        ))
                    }
                </div>
            </div>
            <MyModal
                open={isOpened}
                handleClose={handleClose}
                title={title}
                description={description}
                onSubmit={handleEdit}
            />
        </div>
    )
}


export default PleasureCard;
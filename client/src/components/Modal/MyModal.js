import Modal from "@material-ui/core/Modal";
import Box from "@material-ui/core/Box";
import Typography from "@material-ui/core/Typography";
import {Field, Form, Formik} from "formik";
import schemas from "../../utils/schemas";
import TextInput from "../Formik/TextInput";
import Error from "../Formik/Error";
import Button from "@material-ui/core/Button";
import React from "react";
import {makeStyles} from "@material-ui/core";
import {useSelector} from "react-redux";
import {userSelectors} from "../../redux/user";

const useStyles = makeStyles((theme) => ({
    createBtn: {
        margin: '50px 0',
    },
    modal: {
        position: 'absolute',
        top: '50%',
        left: '50%',
        transform: 'translate(-50%, -50%)',
        width: 400,
        backgroundColor: 'white',
        border: '2px solid #000',
        boxShadow: 24,
        p: 4,
    },
    error: {
        color: theme.palette.error.main,
    },
    submit: {
        margin: theme.spacing(3, 0, 2),
    },
    form: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        margin: '30px'
    },
    modalTitle: {
        margin: '30px'
    }

}))



const MyModal = ({open, handleClose, title, description, onSubmit}) => {

    const classes = useStyles();
    const error = useSelector(userSelectors.error);

    return (
        <Modal
            open={open}
            onClose={handleClose}
            aria-labelledby="modal-modal-title"
            aria-describedby="modal-modal-description"
        >
            <Box className={classes.modal}>
                <Typography className={classes.modalTitle} id="modal-modal-title" variant="h6" component="h2">
                    Edit your pleasure
                </Typography>

                <Formik
                    initialValues={{
                        title: title,
                        description: description
                    }}
                    validationSchema={schemas.createPleasure}
                    onSubmit={onSubmit}
                >
                    {(formikProps) => (
                        <Form noValidate className={classes.form}>
                            <span className={classes.error}>{error}</span>
                            <Field
                                component={TextInput}
                                variant="outlined"
                                margin="normal"
                                multiline
                                fullWidth
                                id="title"
                                label="Pleasure"
                                name="title"
                            />
                            {formikProps.touched.title && (
                                <Error name="title" classes={classes.error}/>
                            )}
                            <Field
                                component={TextInput}
                                variant="outlined"
                                margin="normal"
                                multiline
                                fullWidth
                                name="description"
                                label="Description"
                                id="description"
                            />
                            <Button
                                type="submit"
                                disabled={!formikProps.isValid || formikProps.isSubmitting}
                                variant="contained"
                                color="primary"
                                className={classes.submit}
                            >
                                Save pleasure
                            </Button>
                        </Form>
                    )}
                </Formik>
            </Box>
        </Modal>
    )
}

export default MyModal
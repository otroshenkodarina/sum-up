import React, {useEffect, useState} from 'react';
import Button from "@material-ui/core/Button";
import {makeStyles} from "@material-ui/core";
import Typography from "@material-ui/core/Typography";
import Container from "@material-ui/core/Container";
import {useDispatch, useSelector} from "react-redux";
import {userSelectors} from "../redux/user";
import {pleasuresActions, pleasuresSelectors} from "../redux/pleasures";
import PleasureCard from "../components/PleasureCard/PleasureCard";
import MyModal from "../components/Modal/MyModal";
import Days from "../components/Days/Days";
import {daysActions} from "../redux/days";

const useStyles = makeStyles(() => ({
    createBtn: {
        margin: '30px 0',
    },
    pleasures: {
    },
}))

const Pleasures = () => {
    const [open, setOpen] = useState(false);
    const classes = useStyles();
    const user = useSelector(userSelectors.user);
    const dispatch = useDispatch();
    const pleasures = useSelector(pleasuresSelectors.all)
    const allPleasures = pleasures.map((pleasure, key) => <PleasureCard key={key} card={pleasure} />)
    console.log('pleasures --> ', pleasures)
    useEffect(() => {
        dispatch(pleasuresActions.getAll());
        dispatch(daysActions.getAll());
    }, []);

    const handleOpen = () => setOpen(true);
    const handleClose = () => setOpen(false);

    const handleCreate = async (form, helpers) => {
        const { setSubmitting, resetForm } = helpers;
        form.author = user._id;

        try {
            await dispatch(pleasuresActions.save(form, setSubmitting));
            handleClose()
        } catch (e) {
            /** TODO:
             * throw error action in redux
             * show error message ontop of the form
             */
            console.error(e);
        } finally {
            resetForm();
        }
    }

    return (
        <Container>
            <Typography
                component="h4"
                variant="h2"
                color="textPrimary"
                gutterBottom>
                All your pleasures
            </Typography>
            <Typography color="textSecondary" paragraph>
                Here you can find all of the pleasures you added.
            </Typography>
            <Button
                color="primary"
                variant="outlined"
                className={classes.createBtn}
                onClick={handleOpen}>
                Create pleasure
            </Button>
            <Days />
            <div className={classes.pleasures}>
                {allPleasures}
            </div>
            <MyModal
                open={open}
                handleClose={handleClose}
                title=''
                description=''
                onSubmit={handleCreate}
            />
        </Container>
    )
};

export default Pleasures;
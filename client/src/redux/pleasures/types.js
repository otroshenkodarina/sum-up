const FETCH_PLEASURES = "FETCH_PLEASURES";
const SAVE_PLEASURE = "SAVE_PLEASURE";
const DELETE_PLEASURE = "DELETE_PLEASURE";
const ERROR_PLEASURE = "ERROR_PLEASURE";
const EDIT_PLEASURE = 'EDIT_PLEASURE'

const allActions = {
    FETCH_PLEASURES,
    SAVE_PLEASURE,
    DELETE_PLEASURE,
    ERROR_PLEASURE,
    EDIT_PLEASURE
};

export default allActions;
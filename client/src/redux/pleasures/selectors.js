const allSelectors = {
    all: (store) => store.pleasures.items,
};

export default allSelectors;

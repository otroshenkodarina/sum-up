import types from "./types";

const initialState = {
    items: [],
    error: null,
};

const pleasureReducer = (currentState = initialState, action) => {
    switch (action.type) {
        case types.FETCH_PLEASURES:
            return {
                ...currentState,
                items: action?.payload?.filter(item => item?.title),
            };
        case types.SAVE_PLEASURE:
            const newItems = [...currentState.items];
            newItems.push(action.payload);

            return {
                ...currentState,
                items: newItems,
            };
        case types.ERROR_PLEASURE:
            return {
                ...currentState,
                error: action.payload,
            };
        default:
            return currentState;
    }
};

export default pleasureReducer;

// case types.EDIT_PLEASURE:
//     const

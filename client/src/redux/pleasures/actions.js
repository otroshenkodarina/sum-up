import types from "./types";
import API from "../../utils/API";

const getAll = () => (dispatch, getState) => {
    API.get("/pleasures", getState().user.token).then((r) => {
        dispatch({
            type: types.FETCH_PLEASURES,
            payload: r.data,
        });
    });
};

const save = (newPleasure, setSubmitting) => async (dispatch, getState) => {
    const res = await API.save(
        "/pleasures",
        getState().user.token,
        newPleasure
    );
    dispatch({
        type: types.SAVE_PLEASURE,
        payload: res.data,
    });
    setSubmitting(false);
    dispatch(getAll());
};

const deleteOne = (id) => async (dispatch, getState) => {
    const res = await API.deletePleasure(id, getState().user.token);
    dispatch({
        type: types.DELETE_PLEASURE,
        payload: res.data,
    });
    dispatch(getAll());
};

const allActions = {
    getAll,
    save,
    delete: deleteOne,
    // edit
};

export default allActions;
